set_character_pos 1 -155.265 -7.00002 147.041 22
send_character_message 1 "set_eye_dir -147.566 -7.26144 143.974 1"
send_character_message 1 "set_head_target -147.472 -7.20208 143.969 1"
set_cam -144.257 -7.20578 144.228 0 96.05 0 25.7292
#name "Intro"
#participants 1

say 0 "Ghost" "..."
send_character_message 1 "set_head_target -147.472 -7.20208 143.969 1"
set_character_pos 1 -148.485 -7.00002 143.827 22
set_cam -145.51 -7.17465 147.098 0 36.05 0 25.7292
say 0 "Ghost" "..."
set_cam -145.363 -7.11183 146.707 0 42.06 0 25.7292
send_character_message 1 "set_head_target -147.472 -7.20208 143.969 1"
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_jeer.anm\""
say 1 "Ghost" "Cobus?"
send_character_message 1 "set_head_target -147.472 -7.20208 143.969 1"
send_character_message 1 "set_animation \"Data/Animations/r_actionidle.anm\""
set_cam -146.037 -7.25088 144.788 0 66.05 0 25.7292
say 0 "Ghost" "..."
set_cam -146.037 -7.25088 144.788 0 66.05 0 25.7292
send_character_message 1 "set_head_target -147.472 -7.20208 143.969 1"
send_character_message 1 "set_animation \"Data/Animations/r_dialogue_armcross.anm\""
say 0 "Ghost" "They're dead."
send_character_message 1 "set_head_target -147.642 -6.72525 143.738 1"
set_cam -146.89 -6.45675 143.679 0 96.05 0 25.7292
say 0 "Ghost" "A heavy broadsword wound..."
send_character_message 1 "set_head_target -147.612 -6.72748 143.615 1"
say 0 "Ghost" "Who did this?"
send_character_message 1 "set_eye_dir -147.566 -7.26144 143.974 0"
send_character_message 1 "set_head_target -147.629 -6.56309 143.737 1"
say 0 "Ghost" "The blood is still fresh..."

#name "Meet Gunk2"
#participants 2

say 2 "???" "If you really want nature to reclaim this place..."
say 2 "???" "There's a shrine through this corridor."
say 1 "Ghost" "What do you mean, reclaim?"
say 1 "Ghost" "Do you know something I don't?"
say 2 "???" "Please leave me."
say 2 "???" "Don't think of me when you set that off."

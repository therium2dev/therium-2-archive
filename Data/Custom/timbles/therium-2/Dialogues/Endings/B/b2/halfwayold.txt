set_character_pos 2 71.9986 1.39291 385.703 0
send_character_message 2 "set_eye_dir 66.6124 1.78689 370.815 0.998937"
send_character_message 2 "set_head_target 76.2482 0.776066 363.444 1"
set_character_pos 1 79.7509 0.601658 363.803 180
send_character_message 1 "set_animation \"Data\Animations\r_dialogue_kneelfist.anm\""
send_character_message 1 "set_eye_dir 69.4552 1.05883 363.525 1"
send_character_message 1 "set_head_target 73.0135 1.1821 363.608 1"
set_cam 78.4207 0.642186 365.498 0 -30 0 17.1752
#name "Halfway"
#participants 3

say 1 "Ghost" "...[wait 0.5]christ."
set_cam 81.3723 0.765739 363.29 0 98.06 0 17.1752
set_character_pos 1 79.7509 0.601658 363.803 180
send_character_message 2 "set_eye_dir 66.6124 1.78689 370.815 0.998937"
set_character_pos 2 70.1855 0.822081 364 0
send_character_message 1 "set_head_target 71.9985 0.62548 363.307 1"
send_character_message 2 "set_head_target 76.1409 0.935239 363.508 1"
say 1 "Ghost" "You're difficult."
send_character_message 2 "set_head_target 76.1409 0.935239 363.508 1"
send_character_message 2 "set_eye_dir 66.6124 1.78689 370.815 0.998937"
set_character_pos 1 71.6532 1.67984 385.687 180
send_character_message 1 "set_animation \"Data\Animations\r_combatidle.anm\""
send_character_message 2 "set_animation \"Data\Animations\r_actionidle.anm\""
set_cam 72.3253 1.12182 362.267 0 132.92 0 17.1752
say 2 "Jairo" "Are you ready to go to the Cinderbreathe now?"
send_character_message 2 "set_head_target 76.1409 0.935239 363.508 1"
send_character_message 2 "set_eye_dir 66.6124 1.78689 370.815 0.998937"
set_cam 77.5841 1.14411 363.882 0 -90 0 17.1752
set_character_pos 1 80.1659 0.615642 363.915 180
say 1 "Ghost" "Shut up!"
send_character_message 2 "set_eye_dir 66.6124 1.78689 370.815 0"
send_character_message 2 "set_head_target 74.7682 0.298019 364.027 1"
set_cam 72.1835 1.14418 364.045 0 90 0 17.1752
say 2 "Jairo" "...fine then."

#name "Halfway 2"
#participants 3

say 0 "" "..."
say 2 "Jairo" "Are you done?"
say 1 "Ghost" "Not at all."
say 1 "Ghost" "How about [wait 0.3]you?"
say 2 "Jairo" "I can't stop yet..."
say 2 "Jairo" "They're expending so much energy into fighting you."
say 1 "Ghost" "...[wait 0.5]who?"

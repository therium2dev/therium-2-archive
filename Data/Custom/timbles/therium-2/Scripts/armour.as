void Init() {
}

string displayText;
string changeToChar;
string recoverHealth;
string recoverAll;
float changeRunSpeedAmt;
float changeDmgResistanceAmt;
float changeAttackSpeedAmt;
float changeAttackDmgAmt;

void SetParameters() {
        
params.AddString("Change to Character(Path)","false");
    changeToChar = params.GetString("Change to Character(Path)");

params.AddString("Change Run Speed","0.0");
    changeRunSpeedAmt = params.GetFloat("Change Run Speed");

params.AddString("Change Damage Resistance","0.0");
    changeDmgResistanceAmt = params.GetFloat("Change Damage Resistance");

params.AddString("Change Attack Speed","0.0");
    changeAttackSpeedAmt = params.GetFloat("Change Attack Speed");

params.AddString("Change Attack Damage","0.0");
    changeAttackDmgAmt = params.GetFloat("Change Attack Damage");

}

void HandleEvent(string event, MovementObject @mo){
    if(event == "enter"){
        OnEnter(mo);
    } else if(event == "exit"){
        OnExit(mo);
    }
}

void OnEnter(MovementObject @mo) {
    if(mo.controlled){

//Change to character 
if (changeToChar != "false")
    mo.Execute("SwitchCharacter(\""+changeToChar+"\");");

//Change Player Run Speed
if (changeRunSpeedAmt > 0.0f){
    mo.Execute("run_speed = 8*"+changeRunSpeedAmt+";"); 
    mo.Execute("true_max_speed = 12*"+changeRunSpeedAmt+";");
}

//Change Damage Resistance
if (changeDmgResistanceAmt != 0.0f){
    float damageResistance = 1.0f / changeDmgResistanceAmt;
    mo.Execute("p_damage_multiplier = "+damageResistance+";"); 
}

//Change Attack Speed
if (changeAttackSpeedAmt != 0.0f){
    mo.Execute("p_attack_speed_mult = min(2.0f, max(0.1f, "+changeAttackSpeedAmt+"));"); 
}

//Change Attack Damage
if (changeAttackDmgAmt != 0.0f){
    mo.Execute("p_attack_damage_mult = max(0.0f, "+changeAttackDmgAmt+");"); 
}

}
else{
//NPCs

}
}

void OnExit(MovementObject @mo) {
    if(mo.controlled){
        level.Execute("ReceiveMessage(\"cleartext\")");
    }
}


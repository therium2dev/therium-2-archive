bool played;
string inspect_key = "t";

void Reset() {
    played = false;
}

void Init() {
    Reset();
}

void SetParameters() {
	params.AddIntCheckbox("Check Every Frame", true);
	params.AddIntCheckbox("Play Only If Dead", false);
	params.AddIntCheckbox("Play for NPC's", false);
	params.AddIntCheckbox("Play for Player", true);
	params.AddIntCheckbox("Play on Button", true);
    params.AddString("Text", "Default text");
}

void Update() {
    Object@ obj = ReadObjectFromID(hotspot.GetID());
    vec3 pos = obj.GetTranslation();
    vec3 scale = obj.GetScale();
	if(params.GetInt("Check Every Frame") == 1){
		int num_chars = GetNumCharacters();
		for(int i=0; i<num_chars; ++i){
			MovementObject @mo = ReadCharacter(i);
			
			vec3 mopos = mo.position;
			bool isinside =	   mopos.x > pos.x-scale.x*2.0f
							&& mopos.x < pos.x+scale.x*2.0f
							&& mopos.y > pos.y-scale.y*2.0f
							&& mopos.y < pos.y+scale.y*2.0f
							&& mopos.z > pos.z-scale.z*2.0f
							&& mopos.z < pos.z+scale.z*2.0f;
						
			if(isinside){
				OnEnter(mo);
			}
		}
	}
}

void HandleEvent(string event, MovementObject @mo){
    if(event == "enter"){
        OnEnter(mo);
    } else if(event == "exit"){
        OnExit(mo);
			if(mo.controlled){
			level.SendMessage("cleartext");
		}
    }
}

void OnEnter(MovementObject @mo) {
    if((mo.GetIntVar("knocked_out") > 0 || params.GetInt("Play Only If Dead") == 0)
		&&( (!mo.controlled && params.GetInt("Play for NPC's") == 1)
		|| (mo.controlled && params.GetInt("Play for Player") == 1) && GetInputPressed(mo.controller_id, inspect_key) && params.GetInt("Play on Button") == 1)){
        level.SendMessage("displaytext \""+params.GetString("Text")+"\"");
    }
}

void OnExit(MovementObject @mo) {
}
uint64 global_time; // in ms

// Called by level.cpp at start of level
void Init(string str) {

}

// This script has no need of input focus
bool HasFocus(){
    return false;
}

// This script has no GUI elements
void DrawGUI() {
}

void Update() {
		int num_chars = 0;
		MovementObject@ char = ReadCharacter(num_chars);
		vec3 character_point(char.position.x-RangedRandomFloat(-15.0f,15.0f),char.position.y+30,char.position.z-RangedRandomFloat(-15.0f,15.0f));
		MakeParticle("Data/Custom/timbles/therium-2/Particles/rain.xml",character_point,
				vec3(RangedRandomFloat(-5.0f,5.0f),RangedRandomFloat(-50.0f,-100.0f),RangedRandomFloat(-5.0f,5.0f)));
		MakeParticle("Data/Custom/timbles/therium-2/Particles/rain.xml",character_point,
				vec3(RangedRandomFloat(-5.0f,5.0f),RangedRandomFloat(-50.0f,-100.0f),RangedRandomFloat(-5.0f,5.0f)));
}